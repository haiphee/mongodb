<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Test Database MongoDB</title>
	<link rel="stylesheet" href="">
</head>
<body>
	
	<?php
	   $m = new MongoClient();
	   $db = $m->testdb;
	   $collection = $db->tbl_test;

		if(!$m->testdb){
			echo "สร้าง Databse เรียบร้อยแล้วครับ";
		}
		if (isset($_GET['edit']) <= 0 &&$_SERVER['REQUEST_METHOD'] == 'POST') {
			$name 		= $_POST['firstname'];
			$lastname 	= $_POST['lastname'];
			$mobile 	= $_POST['mobile'];

			$document = array( 
		      "name" => $name, 
		      "lastname" => $lastname, 
		      "mobile" => $mobile
		   	);
		   	$collection->insert($document);
		}
		if(isset($_GET['del']) > 0){
			$collection->remove(array('_id' => new MongoId($_GET['del'])));
		}
		if(isset($_GET['edit']) > 0 && $_SERVER['REQUEST_METHOD'] == 'POST' ){
			$collection->update(
					array("_id"=> new MongoId($_GET['edit'])), 
      				array('$set'=>array(
      					"name"=> $_POST['firstname'] ,
      					"lastname" => $_POST['lastname'],
      					"mobile" => $_POST['mobile']
      				))
      		);
		}
	?>
	<?php 
		if(isset($_GET['edit']) > 0) {
			$row = $collection->findOne(array('_id' => new MongoId($_GET['edit'])));
	?>
	<form action="" method="POST">
		<table border="1">
			<tbody>
				<tr>
					<td><input type="text" name="firstname" required="required" placeholder="กรอกชื่อ" value="<?php echo $row['name'] ;?>"></td>
					<td><input type="text" name="lastname" required="required" placeholder="กรอกนามสกุล" value="<?php echo $row['lastname'] ;?>"></td>
					<td><input type="text" name="mobile" required="required" placeholder="กรอกโทรศัพท์" value="<?php echo $row['mobile'] ;?>"></td>
					<td><input type="submit" value="บันทึกข้อมูล"><input type="hidden" name="_id" value="<?php echo $row['_id'] ;?>"></td>
				</tr>
			</tbody>
		</table>
	</form>

	<?php }else{ ?>

	<form action="" method="POST">
		<table border="1">
			<tbody>
				<tr>
					<td><input type="text" name="firstname" required="required" placeholder="กรอกชื่อ"></td>
					<td><input type="text" name="lastname" required="required" placeholder="กรอกนามสกุล"></td>
					<td><input type="text" name="mobile" required="required" placeholder="กรอกโทรศัพท์"></td>
					<td><input type="submit" value="บันทึกข้อมูล"></td>
				</tr>
			</tbody>
		</table>
	</form>

	<?php } ?>
	<br>
	<table  style="border: 1px solid #cccccc;" cellspacing="5" cellpadding="5" >
		<tbody>
			<?php 
			$row = $collection->find();
			foreach ( $row as $value): ?>
					<tr style="padding: 3px 0px;">
						<td><?php echo $value['name']; ?></td>
						<td><?php echo $value['lastname']; ?></td>
						<td><?php echo $value['mobile']; ?></td>
						<td><a href="?edit=<?=$value['_id']?>">Edit</a></td>
						<td><a href="?del=<?=$value['_id']?>">Delete</a></td>
					</tr>
			<?php endforeach ?>
		</tbody>
	</table>


</body>
</html>